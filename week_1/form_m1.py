class Form:
    _type = ""
    _input = ""
    _button = ""

    def __init__(self, type, input, button):
        self._type = type
        self._input = input
        self._button = button

    def getType(self):
        return self._type.getType()

    def getInput(self):
        return self._input.getInput()

    def getButton(self):
        return self._button.getButton()

class InquiryType:
    _type = ""

    def __init__(self):
        self._type = "inquiry"

    def getType(self):
        return self._type

class InquiryInput:
    _input = ""

    def __init__(self):
        self._input = "query"

    def getInput(self):
        return self._input

class InquiryButton:
    _button = ""

    def __init__(self):
        self._button = "search"
	
    def getButton(self):
        return self._button
class EntryType:
    _type =""
    def __init__(self):
        self._type = "entry"
    def getType(self):
        return self._type
class EntryInput:
    _input = ""
    def __init__(self):
        self._input = "data"
    def getInput(self):
        return self._input
class EntryButton:
    _button = ""
    def __init__(self):
        self._button = "save"
    def getButton(self):
        return self._button
def main():
    inquiryType = InquiryType()
    inquiryInput = InquiryInput()
    inquiryButton = InquiryButton()

    inquiryForm = Form(inquiryType, inquiryInput, inquiryButton)
    print("Form type ", inquiryForm.getType())
    print("Form input ", inquiryForm.getInput())
    print("Form button ", inquiryForm.getButton())
    
    entryType = EntryType()
    entryInput = EntryInput()
    entryButton = EntryButton()

    entryForm = Form(entryType, entryInput, entryButton)
    print("Form type ", entryForm.getType()) #will print "entry"
    print("Form input ", entryForm.getInput()) #will print "data"
    print("Form button ", entryForm.getButton()) #will print "save"
