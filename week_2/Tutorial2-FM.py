#               Mandatory 3: Create a program so it implement Factory Method Pattern. There are a template class to use Factory Method Pattern but you are free to explore your idea in implementing Factory Method Pattern.

def main():
        board = Board3x3()
        print (board)

class AbstractBoard:
        def __init__(self, rows, columns):
                self.board = [[None for _ in range(columns)] for _ in range(rows)]
                self.populate_board()
        def populate_board(self):
                raise NotImplementedError()
        def __str__(self):
                squares = []
                for x, row in enumerate(self.board):
                        for y, column in enumerate(self.board):
                                squares.append(self.board[x][y])
                        squares.append("\n")
                return "".join(squares)

class Board3x3(AbstractBoard):
                def __init__(self):
                                super().__init__(4, 4)
                                
                def populate_board(self):
                        for row in range(4):
                                if row%2==0:
                                        for column in range(4):
        
                                                if (column%2): self.board[row][column] = create_piece("o") 
                                                else: self.board[row][column] = create_piece("x")
                                else:
                                        for column in range(4):
                                                if (column%2): self.board[row][column] = create_piece("u")
                                                else: self.board[row][column] = create_piece("v")

# Uncomment the codes below
class Piece(str):
         __slots__ = ()
                
class Circle(Piece):
        __slots__ = ()
        def __new__(Piece):
                return super().__new__(Piece, "o")
# Mandatory 6: Implement the Factory Method in here

class Cross(Piece):
        __slots__ = ()
        def __new__(Piece):
                return super().__new__(Piece, "x")
class u_shape(Piece):
        __slots__ = ()
        def __new__(Piece):
                return super().__new__(Piece, "u")
class v_shape(Piece):
        __slots__ = ()
        def __new__(Piece):
                return super().__new__(Piece, "v")
def create_piece(jenis):
        if jenis == "x":
                return Cross()
        elif jenis == "v":
                return v_shape()
        elif jenis == "o":
                return Circle()
        elif jenis == "u":
                return u_shape()
                
        
        
# Mandatory 6: Implement the Factory Method in here

if __name__ == "__main__":
    main()
                                                                
