#	Mandatory 9: Edit IdnCurrRates class to a Singleton class
#	Mandatory 10: Edit IdnCurrRates class to be able to run with Borg class
class IdnCurrRates (object):
        class _IdnCurrRates:
                def __init__(self,rates):
                        self.rates = rates
                def __str__(self):
                        return str(self.rates)
        inst = None
        def __init__(self,rates):
                if not IdnCurrRates.inst:
                        IdnCurrRates.inst = IdnCurrRates._IdnCurrRates(rates)
                else:
                        IdnCurrRates.inst.rates = rates

#	Mandatory 10: Create Borg class then create SgprCurrRates class that has the same method as IdnCurrRates

class Borg:
        _shared_state = {}
        def __init__(self):
                self.__dict__=self._shared_state
#Implemented in here

class SgprCurrRates(Borg):
        class _SgprCurrRates:
                
                def __init__(self,rates):
                        self.rates = rates
                def __str__(self):
                        return str(self.rates)
        inst = None
        def __init__(self,rates):
                if not SgprCurrRates.inst:
                        SgprCurrRates.inst = SgprCurrRates._SgprCurrRates(rates)
                else:
                        SgprCurrRates.inst.rates = rates
#implemented in here
		
x = IdnCurrRates(10000)
print ('Rates x: '+ str(x.inst))
y = IdnCurrRates(20000)
print ('Rates y: ' + str(y.inst))
z = IdnCurrRates(30000)
print ('Rates z: ' + str(z.inst))
print ('X and Y condition should be 30000')
print ('Rates x: '+ str(x.inst))
print ('Rates y: ' + str(y.inst))

# Below are codes for Mandatory 10
e = SgprCurrRates(40000)
print ('Rates e: '+ str(e.inst))
f = SgprCurrRates(50000)
print ('Rates f: ' + str(f.inst))
g = SgprCurrRates(60000)
print ('Rates g: ' + str(g.inst))
print ('E and F condition should be 60000')
print ('Rates e: '+ str(e.inst))
print ('Rates f: ' + str(f.inst))
